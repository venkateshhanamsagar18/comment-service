package com.facebook.comment.controller;

import com.facebook.comment.model.CommentModel;
import com.facebook.comment.model.CommentPostModel;
import com.facebook.comment.service.ICommentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@Slf4j
@RestController
@RequestMapping("/v1.0/comment")
public class CommentController {
      @Autowired
      private ICommentService commentService;

  //    @PostMapping("/signup")
  //    public ResponseEntity<Object> createUser(@RequestBody UserModel userModel) {
  //        log.info("Inside createUser, got userModel : {}", userModel);
  //        boolean isSuccess = userServiceImpl.create(userModel);
  //        if (isSuccess) {
  //            log.info("Success status: {}", isSuccess);
  //            return ResponseEntity.ok("User created successfully");
  //        } else {
  //            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
  //                    .body("User with this email already exists");
  //        }
  //    }

  @PostMapping("/addComment/{userId}/{postId}/{content}")
  public ResponseEntity<Object> createComment(@PathVariable int userId, @PathVariable String postId, @PathVariable String content) {

//    log.info("Inside createUser, got userModel : {}", userModel);
      CommentModel commentModel=new CommentModel();
      commentModel.setPostId(postId);
      commentModel.setUserId(userId);
      commentModel.setContent(content);
    boolean isSuccess = commentService.createComment(commentModel);
    if (isSuccess) {
      log.info("Success status: {}", isSuccess);
      return ResponseEntity.ok("User created successfully");
    } else {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST)
          .body("User with this email already exists");
    }
  }
    @GetMapping("/getCommentByPostId/{postId}")
    public List<CommentPostModel> getCommentByPostId(@PathVariable String postId) {
      log.info("Getting comments");
      return commentService.getCommentByPostId(postId);
    }


}
