package com.facebook.comment.service;

import com.facebook.comment.entity.Comment;
import com.facebook.comment.model.CommentModel;
import com.facebook.comment.model.CommentPostModel;
import com.facebook.comment.repository.CommentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

@Slf4j
@Service
public class CommentService implements ICommentService {

    @Autowired
    CommentRepository commentRepository;
    @Autowired
    private RestTemplate restTemplate;
    @Override
    public Boolean createComment(CommentModel commentModel) {
        Comment comment=new Comment();
        comment.setContent(commentModel.getContent());
        comment.setPostId(commentModel.getPostId());
        comment.setUserId(commentModel.getUserId());
        comment.setTime(LocalDateTime.now());

        commentRepository.save(comment);

        return true;
    }
    public List<CommentPostModel> getCommentByPostId(String postId){
        log.info("Got call first time");
            List<CommentPostModel> commentPostModels = new ArrayList<>();
            Optional<List<Comment>> comments = commentRepository.findByPostIdOrderByIdDesc(postId);

    if (comments.isPresent()) {
      for (Comment comment : comments.get()) {
          log.info("GOT CALL HERE");
          String name = restTemplate.postForObject("http://localhost:8081/v1.0/users/getNameByUserId",comment.getUserId(),String.class);
        CommentPostModel commentPostModel = new CommentPostModel();
        commentPostModel.setUserId(comment.getUserId());
        commentPostModel.setName(name);
        commentPostModel.setContent(comment.getContent());
        commentPostModel.setTime(comment.getTime());
        commentPostModels.add(commentPostModel);
      }
            }
            return commentPostModels;
    }
}
