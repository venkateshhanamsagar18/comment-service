package com.facebook.comment.service;

import com.facebook.comment.model.CommentModel;
import com.facebook.comment.model.CommentPostModel;

import java.util.List;

public interface ICommentService {
    Boolean createComment(CommentModel commentModel);
    List<CommentPostModel> getCommentByPostId(String postId);
}
