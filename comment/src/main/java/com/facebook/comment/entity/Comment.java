package com.facebook.comment.entity;

import jakarta.persistence.*;
import lombok.*;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "comment_table")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Comment {
  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "user_id")
  private int userId;

  @Column(name = "post_id")
  private String postId;

  @Column(name = "content")
  private String content;

  @Column(name = "time")
  private LocalDateTime time;

  public int getId() {
    return id;
  }

  public int getUserId() {
    return userId;
  }

  public String getPostId() {
    return postId;
  }

  public String getContent() {
    return content;
  }

  public LocalDateTime getTime() {
    return time;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public void setPostId(String postId) {
    this.postId = postId;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setTime(LocalDateTime time) {
    this.time = time;
  }
}
