package com.facebook.comment.repository;

import com.facebook.comment.entity.Comment;
import com.facebook.comment.model.CommentPostModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CommentRepository extends JpaRepository<Comment,Integer> {
Optional<List<Comment>> findByPostIdOrderByIdDesc(String postId);
}
